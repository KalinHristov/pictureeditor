#include "tablemodel.h"
#include <QIcon>
#include <QPixmap>
#include <QSize>
#include <QStringListModel>

TableModel::TableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QList< Picture* > TableModel::pictures() {
    return m_pictures;
}

int TableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_pictures.size();
}

int TableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return ColumnsCount;
}

#include <QDir>

QVariant TableModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    const int rowIndex = index.row();
    if ( rowIndex >= m_pictures.size()
         || rowIndex < 0 )
    {
        return QVariant();
    }

    switch (index.column())
    {
        case ColumnIndexImage:
        {
            switch ( role )
            {
                case Qt::DisplayRole: return QDir::toNativeSeparators( m_pictures.at(index.row())->path() );
                case Qt::DecorationRole: return m_pictures.at(index.row())->image();
            }

            break;
        }
        case ColumnIndexStatus:
        {
            switch ( role )
            {
                case Qt::DisplayRole: return m_pictures.at(index.row())->statusDescription();
                case Qt::DecorationRole: return m_pictures.at(index.row())->statusImage();
            }

            break;
        }
    }

    return QVariant();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    if ( orientation == Qt::Horizontal )
    {
        switch(section)
        {
            case ColumnIndexImage:
                return tr("Image");
            case ColumnIndexStatus:
                return tr("Status");
        }

        return QVariant();
    }

    return this->QAbstractTableModel::headerData( section, orientation, role );
}

void TableModel::addPicture(Picture* aPicture)
{
    beginInsertRows( QModelIndex() , rowCount(), rowCount() );
    m_pictures.push_back(aPicture);
    endInsertRows();
}

void TableModel::removeSelectedRows(QModelIndexList rows) {
    qSort(rows.begin(), rows.end(), qGreater<QModelIndex>());

    for(int i = 0; i < rows.size(); i++)
    {
        int row = rows.at(i).row();

        beginRemoveRows(QModelIndex(), row, row);
        m_pictures.removeAt(row);
        endRemoveRows();
    }
}

void TableModel::removeAllRows() {
    if (m_pictures.size() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
        m_pictures.clear();
        endRemoveRows();
    }
}

void TableModel::dataUpdated() {
    emit dataChanged(QModelIndex(), QModelIndex());
}
