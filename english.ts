<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <source>Save in folder</source>
        <translation>Save in folder</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="74"/>
        <location filename="mainwindow.cpp" line="346"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>Select Picture</source>
        <translation>Select Picture</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Open Directory</source>
        <translation>Open Directory</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <location filename="mainwindow.cpp" line="344"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <location filename="mainwindow.cpp" line="345"/>
        <source>Bulgarian</source>
        <translation>Bulgarian</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="effect.cpp" line="22"/>
        <source>red</source>
        <translation>red</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="23"/>
        <source>green</source>
        <translation>green</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="24"/>
        <source>blue</source>
        <translation>blue</translation>
    </message>
    <message>
        <location filename="effect.cpp" line="25"/>
        <source>black</source>
        <translation>black</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>%1/%2_new.jpg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="picture.cpp" line="33"/>
        <source>Pending</source>
        <translation>Pending</translation>
    </message>
    <message>
        <location filename="picture.cpp" line="34"/>
        <source>In Progress ...</source>
        <translation>In Progress ...</translation>
    </message>
    <message>
        <location filename="picture.cpp" line="35"/>
        <source>Ready</source>
        <translation>Ready</translation>
    </message>
</context>
<context>
    <name>TableModel</name>
    <message>
        <location filename="tablemodel.cpp" line="77"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="tablemodel.cpp" line="79"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
</context>
</TS>
