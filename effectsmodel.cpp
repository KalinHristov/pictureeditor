#include "effectsmodel.h"

EffectsModel::EffectsModel(QObject *parent)
    : QAbstractListModel(parent)
{

}

int EffectsModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_effects.size();
}

QVariant EffectsModel::data(const QModelIndex &index, int role) const
{
    const int rowIndex = index.row();
    if ( rowIndex >= m_effects.size()
         || rowIndex < 0 )
    {
        return QVariant();
    }

    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        return m_effects.at(index.row()).typeDescription();
    }

    return QVariant();
}

void EffectsModel::addEffect(Effect aEffect)
{
    beginInsertRows( QModelIndex() , rowCount(), rowCount() );
    m_effects.push_back(aEffect);
    endInsertRows();
}
