#include "effecttask.h"

#include <QImage>
#include <QPixmap>
#include <QDebug>
#include <QThread>
#include <QTime>

EffectTask::EffectTask(Picture* aPicture, QString aDirrectory, QString aEffect)
{
    m_picture = aPicture;
    m_directory = aDirrectory;
    m_effect = aEffect;
}

void EffectTask::run()
{
    //m_picture->setStatus("In progress");
    emit started();
    QImage* original = new QImage(m_picture->GetData(0).toString());
    int width = original->width();
    int height = original->height();

    QImage* newPicture = new QImage(width, height, QImage::Format_RGB32);

    QColor rgb;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            rgb = original->pixel(i, j);
            int red = rgb.red();
            int green = rgb.green();
            int blue = rgb.blue();
            int black = (red + green + blue) / 3;

            if (m_effect == "red") {
                newPicture->setPixelColor(i, j, QColor(255, red, red));
            }
            else if (m_effect == "green") {
                newPicture->setPixelColor(i, j, QColor(green, 255, green));
            }
            else if (m_effect == "blue") {
                newPicture->setPixelColor(i, j, QColor(blue, blue, 255));
            }
            else if (m_effect == "black") {
                newPicture->setPixelColor(i, j, QColor(black, black, black));
            }
        }
    }

    QPixmap newPicturePixmap = QPixmap::fromImage(*newPicture);
    if (QString(m_directory).isEmpty()) {
        m_directory = m_picture->GetData(0).toString().section("/",0,-2);
    }
    newPicturePixmap.save(tr("%1/%2_new.jpg").arg(m_directory).arg(m_picture->GetData(0).toString().section("/",-1,-1).section(".",0,-2)));

    //m_picture->setStatus("Ready");
    emit finished();
}

void EffectTask::inProgress() {
    m_picture->setStatus("In progress");
    qDebug() << QTime::currentTime().toString() << "in progress";
}

void EffectTask::ready() {
    m_picture->setStatus("Ready");
    qDebug() << QTime::currentTime().toString() << "ready";
}
