#include "picture.h"

Picture::Picture(const QString& aPath)
    : m_path( aPath )
    , m_pixmap( aPath )
    , m_status( Status::Pending )
{
}

QString Picture::path() const {
    return m_path;
}

const QIcon&Picture::image() const
{
    return m_pixmap;
}

void Picture::setStatus(const Picture::Status aStatus)
{
    m_status = aStatus;
}

Picture::Status Picture::status() const
{
    return m_status;
}

QString Picture::statusDescription() const
{
    switch ( m_status )
    {
        case Status::Pending: return QObject::tr( "Pending" );
        case Status::InProgress: return QObject::tr( "In Progress ..." );
        case Status::Ready: return QObject::tr( "Ready" );
    }

    return QString();
}

QIcon Picture::statusImage() const
{
    switch ( m_status )
    {
        case Status::Pending: return QIcon( ":/images/pending.png" );
        case Status::InProgress: return QIcon( ":/images/in_progress.png" );
        case Status::Ready: return QIcon( ":/images/ready.png" );
    }

    return QIcon();
}
