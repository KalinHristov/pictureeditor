QT += core gui
QT += widgets

LIBS += -lQt5Concurrent

HEADERS += \
    mainwindow.h \
    tablemodel.h \
    picture.h \
    effectsmodel.h \
    effect.h

SOURCES += \
    mainwindow.cpp \
    main.cpp \
    tablemodel.cpp \
    picture.cpp \
    effectsmodel.cpp \
    effect.cpp

RESOURCES += \
    images.qrc \
    languages.qrc

TRANSLATIONS += english.ts \
    bulgarian.ts

CODECFORTR = UTF-8

DISTFILES +=
