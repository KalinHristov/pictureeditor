#include "effect.h"
#include <QObject>

Effect::Effect(Type aType)
    : m_type( aType )
{
}

Effect::Type Effect::type() const {
    return m_type;
}

void Effect::setType(const Effect::Type aType)
{
    m_type = aType;
}

QString Effect::typeDescription() const
{
    switch ( m_type )
    {
        case Type::Red: return QObject::tr( "red" );
        case Type::Green: return QObject::tr( "green" );
        case Type::Blue: return QObject::tr( "blue" );
        case Type::Black: return QObject::tr( "black" );
    }

    return QString();
}
