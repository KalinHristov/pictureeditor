#include "mainwindow.h"
#include "effecttask.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPixmap>
#include <QFileDialog>
#include <QImage>
#include <QColor>
#include <QHeaderView>
#include <QStringListModel>
#include <QThreadPool>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QDragEnterEvent>
#include <QMenuBar>
#include <QTranslator>
#include <QApplication>

const QSet< QString > MainWindow::s_supportedImageFormats = QSet< QString >() << "png" << "jpg" << "jpeg" << "bmp";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_tableModel(new TableModel)
    , m_effectModel(new EffectsModel)
    , m_table(new QTableView)
    , m_progressBar(new QProgressBar)
    , m_saveInFolder(new QCheckBox)
    , m_addImage(new QPushButton)
    , m_removeImage(new QPushButton)
    , m_removeAllImages(new QPushButton)
    , m_effects(new QComboBox)
    , m_effect(Effect::Type::Red)
    , m_before(new QLabel)
    , m_after(new QLabel)
    , m_start(new QPushButton)
{
    createActions();
    createMenus();

    this->setAcceptDrops(true);

    m_table->setModel(m_tableModel);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_table->verticalHeader()->hide();
    m_table->setAcceptDrops(true);
    m_table->setDropIndicatorShown(true);
    m_table->setTextElideMode(Qt::ElideMiddle);

    m_progressBar->setTextVisible(false);

    connect(m_progressBar, SIGNAL(valueChanged(int)), this, SLOT(progressBarValueChanged()));

    m_saveInFolder->setText(tr("Save in folder"));

    buttonStyle(m_addImage, "plus.png");
    buttonStyle(m_removeImage, "minus.png");
    buttonStyle(m_removeAllImages, "delete.png");

    connect(m_addImage, SIGNAL(clicked()), this, SLOT(addImageClicked()));
    connect(m_removeImage, SIGNAL(clicked()), this, SLOT(removeImageClicked()));
    connect(m_removeAllImages, SIGNAL(clicked()), this, SLOT(removeAllImagesClicked()));

    m_effectModel->addEffect(Effect(Effect::Type::Red));
    m_effectModel->addEffect(Effect(Effect::Type::Green));
    m_effectModel->addEffect(Effect(Effect::Type::Blue));
    m_effectModel->addEffect(Effect(Effect::Type::Black));

    m_effects->setModel(m_effectModel);

    m_before->setPixmap(QPixmap(":/images/The_Mario_Bros.jpeg"));
    m_after->setPixmap(QPixmap(":/images/red.jpg"));

    connect(m_effects, SIGNAL(currentIndexChanged(int)), this, SLOT(effectChanged(int)));

    m_start->setText(tr("Start"));

    connect(m_start, SIGNAL(clicked()), this, SLOT(startClicked()));

    QHBoxLayout* const hLayout = new QHBoxLayout;
    hLayout->addWidget(m_saveInFolder);
    hLayout->addWidget(m_addImage);
    hLayout->addWidget(m_removeImage);
    hLayout->addWidget(m_removeAllImages);

    QHBoxLayout* const effectsLayout = new QHBoxLayout;
    effectsLayout->addWidget(m_effects);
    effectsLayout->addWidget(m_before);
    effectsLayout->addWidget(m_after);

    QVBoxLayout* const mainLayout = new QVBoxLayout();
    mainLayout->addWidget(m_table);
    mainLayout->addWidget(m_progressBar);
    mainLayout->addLayout(hLayout);
    mainLayout->addLayout(effectsLayout);
    mainLayout->addWidget(m_start);

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);

    setCentralWidget(window);
}

void MainWindow::effectChanged(int aEffect) {
    switch (aEffect)
    {
    case Effect::Type::Red:
        m_effect.setType(Effect::Type::Red);
        m_after->setPixmap(QPixmap(":/images/red.jpg"));
        break;
    case Effect::Type::Green:
        m_effect.setType(Effect::Type::Green);
        m_after->setPixmap(QPixmap(":/images/green.jpg"));
        break;
    case Effect::Type::Blue:
        m_effect.setType(Effect::Type::Blue);
        m_after->setPixmap(QPixmap(":/images/blue.jpg"));
        break;
    case Effect::Type::Black:
        m_effect.setType(Effect::Type::Black);
        m_after->setPixmap(QPixmap(":/images/black.jpg"));
        break;
    }
}

void MainWindow::addImageClicked() {
    const QString separator = "*.";
    const QString filter = tr( "File Description (%1%2)" ).arg( separator ).arg( QStringList( s_supportedImageFormats.toList() ).join( ' ' + separator ) );

    QStringList filenames = QFileDialog::getOpenFileNames(this, tr("Select Picture") ,QDir::homePath(), filter);
    if( !filenames.isEmpty() )
    {
        for (int i = 0; i < filenames.count(); i++) {
            m_tableModel->addPicture(new Picture(filenames.at(i)));
        }
    }
}

void MainWindow::removeImageClicked() {
    QModelIndexList rows = m_table->selectionModel()->selectedRows();
    m_tableModel->removeSelectedRows(rows);
}

void MainWindow::removeAllImagesClicked() {
    m_tableModel->removeAllRows();
}

void MainWindow::startClicked() {
    m_progressBar->setValue(0);

    const QList< Picture* > imageConverters = m_tableModel->pictures();

    if (imageConverters.empty())
    {
        return;
    }

    m_start->setDisabled(true);
    m_addImage->setDisabled(true);
    m_removeImage->setDisabled(true);
    m_removeAllImages->setDisabled(true);
    m_effects->setDisabled(true);
    m_saveInFolder->setDisabled(true);

    QString directory = "";

    if (m_saveInFolder->isChecked())
    {
        directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home"
                                                        , QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
    }

    m_imageConvertersSize = imageConverters.size();

    for ( Picture* const imageConverter : imageConverters )
    {
        QFutureWatcher<void>* const futureWatcher = new QFutureWatcher< void >( this );
        connect(futureWatcher, SIGNAL(finished()), SLOT(processImageConversionFinished()));

        m_imageConverters[ futureWatcher ] = imageConverter;

        futureWatcher->setFuture(QtConcurrent::run(&MainWindow::useEffect, imageConverter, m_effect, directory));

        imageConverter->setStatus( Picture::Status::InProgress );
        m_tableModel->dataUpdated();
    }
}

void MainWindow::useEffect(Picture* aPicture, Effect aEffect, QString aDirectory)
{
    QImage *original = new QImage(aPicture->path());
    int width = original->width();
    int height = original->height();

    QImage *newPicture = new QImage(width, height, QImage::Format_RGB32);

    QColor rgb;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            rgb = original->pixel(i, j);
            int red = rgb.red();
            int green = rgb.green();
            int blue = rgb.blue();
            int black = (red + green + blue) / 3;

            if (aEffect.type() == Effect::Type::Red)
            {
                newPicture->setPixelColor(i, j, QColor(255, red, red));
            }
            else if (aEffect.type() == Effect::Type::Green)
            {
                newPicture->setPixelColor(i, j, QColor(green, 255, green));
            }
            else if (aEffect.type() == Effect::Type::Blue)
            {
                newPicture->setPixelColor(i, j, QColor(blue, blue, 255));
            }
            else if (aEffect.type() == Effect::Type::Black)
            {
                newPicture->setPixelColor(i, j, QColor(black, black, black));
            }
        }
    }

    QPixmap newPicturePixmap = QPixmap::fromImage(*newPicture);

    if (QString(aDirectory).isEmpty())
    {
        aDirectory = aPicture->path().section("/",0,-2);
    }
    newPicturePixmap.save(QObject::tr("%1/%2_new.jpg")
                          .arg(aDirectory)
                          .arg(aPicture->path()
                               .section("/",-1,-1)
                               .section(".",0,-2)));
}

void MainWindow::processImageConversionFinished()
{
    QFutureWatcher< void >* const watcher = dynamic_cast< QFutureWatcher< void >* >( this->sender() );
    if ( watcher == Q_NULLPTR )
    {
        return;
    }

    m_imageConverters.take(watcher)->setStatus(Picture::Status::Ready);
    m_tableModel->dataUpdated();
    m_imageConverters.remove( watcher );

    m_imageConvertersSize--;

    const int picturesTotalCount = m_tableModel->pictures().size();
    m_progressBar->setValue( 100 - (m_imageConvertersSize * 100 / picturesTotalCount) );
}

void MainWindow::progressBarValueChanged()
{
    if (m_progressBar->value() == 100)
    {
        m_start->setEnabled(true);
        m_addImage->setEnabled(true);
        m_removeImage->setEnabled(true);
        m_removeAllImages->setEnabled(true);
        m_effects->setEnabled(true);
        m_saveInFolder->setEnabled(true);
    }
}

void MainWindow::buttonStyle(QPushButton* aButton, QString aPath)
{
    QDir directory(":/images");

    aButton->setMaximumWidth(30);
    aButton->setIcon(QIcon(directory.filePath(aPath)));
    aButton->setStyleSheet("border-style: none;");
    aButton->setIconSize(QSize(24,24));
}

bool MainWindow::isValidDragAndDropEvent( QDropEvent* const aDragAndDropEvent )
{
    return aDragAndDropEvent->mimeData()->hasUrls();
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if ( MainWindow::isValidDragAndDropEvent( event ) )
    {
        event->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent *event)
{
    if (! MainWindow::isValidDragAndDropEvent( event ))
    {
        return;
    }

    for (const QUrl& url: event->mimeData()->urls())
    {
        if ( ! url.isLocalFile() )
        {
            continue;
        }

        const QString localFilePath = url.toLocalFile();
        const QFileInfo fi( localFilePath );
        if ( ! fi.exists() )
        {
            continue;
        }

        const QString format = fi.suffix().toLower();
        if ( ! s_supportedImageFormats.contains( format ) )
        {
            continue;
        }

        Picture* const picture = new Picture( localFilePath );
//        if ( ! picture->isValid() )
//        {
//            delete picture;
//            continue;
//        }

        m_tableModel->addPicture(picture);
    }
}

void MainWindow::createActions()
{
    m_english = new QAction(tr("English"), this);
    m_english->setIcon(QIcon(":/images/gb.png"));

    m_bulgarian = new QAction(tr("Bulgarian"), this);
    m_bulgarian->setIcon(QIcon(":/images/bg.png"));

    connect(m_english, SIGNAL(triggered(bool)), this, SLOT(english()));
    connect(m_bulgarian, SIGNAL(triggered(bool)), this, SLOT(bulgarian()));


    m_languageGroup = new QActionGroup(this);
    m_languageGroup->addAction(m_english);
    m_languageGroup->addAction(m_bulgarian);
}

void MainWindow::createMenus()
{
    m_menu = menuBar()->addMenu(tr("Language"));
    m_menu->addAction(m_english);
    m_menu->addAction(m_bulgarian);
}

void MainWindow::bulgarian()
{
    m_translator.load(":/languages/bulgarian.qm");
    qApp->installTranslator(&m_translator);
}

void MainWindow::english()
{
    m_translator.load(":/languages/english.qm");
    qApp->installTranslator(&m_translator);
}

void MainWindow::changeEvent( QEvent *event )
{
    if( event->type() == QEvent::LanguageChange )
    {
        m_english->setText(tr("English"));
        m_bulgarian->setText(tr("Bulgarian"));
        m_menu->setTitle(tr("Language"));
        m_start->setText(tr("Start"));
        m_saveInFolder->setText(tr("Save in folder"));
    }
}


