#ifndef EFFECTSMODEL_H
#define EFFECTSMODEL_H

#include <QAbstractListModel>

#include "effect.h"

class EffectsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit EffectsModel( QObject *parent = Q_NULLPTR );

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    void addEffect( Effect aEffect );

private:
    QList< Effect > m_effects;
};

#endif // EFFECTSMODEL_H
