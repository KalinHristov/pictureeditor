#ifndef EFFECTTHREAD_H
#define EFFECTTHREAD_H

#include "picture.h"

#include <QRunnable>

class EffectTask : public QObject, public QRunnable
{
    Q_OBJECT

public:
    EffectTask(Picture* aPicture,  QString aDirectory, QString aEffect);
    void run();

signals:
    void started();
    void finished();

public slots:
    void inProgress();
    void ready();

private:
    Picture* m_picture;
    QString m_directory;
    QString m_effect;
};

#endif // EFFECTTHREAD_H
